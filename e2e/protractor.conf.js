const url = 'http://localhost:4200/';
const path = require('path');
const HtmlReporter = require('protractor-beautiful-reporter');

require('ts-node/register');

exports.config = {
    baseUrl: url,
    specs: ['../src/e2e/**/*.e2e.ts'],
    exclude: [],
    framework: 'jasmine2',
    resultJsonOutputFile: './result.json',
    allScriptsTimeout: 12000,
    jasmineNodeOpts: {
        showTiming: true,
        showColors: true,
        isVerbose: false,
        includeStackTrace: false,
        defaultTimeoutInterval: 40000
    },
    directConnect: true,
    chromeOnly: false,
    multiCapabilities: [
        {
            'browserName': 'chrome',
            'chromeOptions': {
                'args': ['--disable-gpu', '--window-size=1600,1000', '--headless']
                // For hide tests: '--headless', '--disable-gpu', '--no-sandbox'
            },
        },
    ],
    maxSessions: 1,
    onPrepare() {
        jasmine.getEnv().addReporter(new HtmlReporter({
            preserveDirectory: false,
            takeScreenShotsOnlyForFailedSpecs: true,
            screenshotsSubfolder: 'images',
            jsonsSubfolder: 'jsons',
            baseDirectory: 'reports-tmp',
            sortFunction: function sortFunction(a, b) {
                if (a.cachedBase === undefined) {
                    let aTemp = a.description.split('|').reverse();
                    a.cachedBase = aTemp.slice(0).slice(0,-1);
                    a.cachedName = aTemp.slice(0).join('');
                }
                if (b.cachedBase === undefined) {
                    let bTemp = b.description.split('|').reverse();
                    b.cachedBase = bTemp.slice(0).slice(0,-1);
                    b.cachedName = bTemp.slice(0).join('');
                }

                let firstBase = a.cachedBase;
                let secondBase = b.cachedBase;

                for (let i = 0; i < firstBase.length || i < secondBase.length; i++) {

                    if (firstBase[i] === undefined) { return -1; }
                    if (secondBase[i] === undefined) { return 1; }
                    if (firstBase[i].localeCompare(secondBase[i]) === 0) { continue; }
                    return firstBase[i].localeCompare(secondBase[i]);
                }

                let firstTimestamp = a.timestamp;
                let secondTimestamp = b.timestamp;

                if(firstTimestamp < secondTimestamp) return -1;
                else return 1;
            },

            pathBuilder: function pathBuilder(spec, descriptions, results, capabilities) {
                let currentDate = new Date(),
                    day = currentDate.getDate(),
                    month = currentDate.getMonth() + 1,
                    year = currentDate.getFullYear();

                let validDescriptions = descriptions.map(function (description) {
                    return description.replace('/', '@');
                });

                return path.join(
                    day + "-" + month + "-" + year,
                    validDescriptions.join('-'));
            }
        }).getJasmine2Reporter());

        require('ts-node').register({
            project: require('path').join(__dirname, './tsconfig.e2e.json')
        });
    },
    useAllAngular2AppRoots: false,
};
