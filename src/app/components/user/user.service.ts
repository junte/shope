import {Injectable} from '@angular/core';

import {getMock} from '@junte/mocker';
import {Observable, of} from 'rxjs';
import {delay} from 'rxjs/operators';

import {MOCKS_DELAY} from '../../../consts';
import {AccessToken} from '../../../models/access-token';
import {User, UserCredentials, UserUpdated} from '../../../models/user';

@Injectable({
    providedIn: 'root',
})
export class UserService {
    constructor() {
    }

    get(): Observable<User> {
        return of(getMock(User))
            .pipe(delay(MOCKS_DELAY));
    }

    login(credentials: UserCredentials): Observable<AccessToken> {
        return of(getMock(AccessToken))
            .pipe(delay(MOCKS_DELAY));
    }

    logout(): Observable<any> {
        return of(null)
            .pipe(delay(MOCKS_DELAY));
    }

    update(userUpdated: UserUpdated): Observable<User> {
        const user = getMock(User);
        Object.assign(user, userUpdated);
        return of(user)
            .pipe(delay(MOCKS_DELAY));
    }
}
