import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {EsanumUiModule} from '@esanum/ui';

import {ProfileComponent} from './profile.component';

@NgModule({
    declarations: [ProfileComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: ProfileComponent,
            },
        ]),
        EsanumUiModule,
    ],
})
export class ProfileModule {
}
