import {Component, OnInit} from '@angular/core';

import {UI} from '@esanum/ui';
import {finalize} from 'rxjs/operators';

import {User} from '../../../../models/user';
import {UserService} from '../user.service';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
    ui = UI;

    user: User;
    loading = false;

    constructor(
        private userService: UserService,
    ) {
    }

    ngOnInit(): void {
        this.loading = true;
        this.userService.get()
            .pipe(finalize(() => this.loading = false))
            .subscribe(user => this.user = user);
    }

}
