import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EsanumUiModule} from '@esanum/ui';

import {UserRoutingModule} from './user-routing.module';
import {UserComponent} from './user.component';

@NgModule({
    declarations: [UserComponent],
    imports: [
        CommonModule,
        UserRoutingModule,
        EsanumUiModule,
    ],
})
export class UserModule {
}
