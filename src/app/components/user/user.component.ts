import {Component, OnInit} from '@angular/core';

import {UI} from '@esanum/ui';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
    ui = UI;

    constructor() {
    }

    ngOnInit(): void {
    }

}
