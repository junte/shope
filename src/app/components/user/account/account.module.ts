import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {EsanumUiModule} from '@esanum/ui';

import {AccountComponent} from './account.component';

@NgModule({
    declarations: [AccountComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: AccountComponent,
            },
        ]),
        EsanumUiModule,
        FormsModule,
        ReactiveFormsModule,
    ],
})
export class AccountModule {
}
