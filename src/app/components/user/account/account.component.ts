import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';

import {FormComponent, UI} from '@esanum/ui';
import {finalize} from 'rxjs/operators';

import {User, UserUpdated} from '../../../../models/user';
import {SignalsService} from '../../../../services/signals.service';
import {UserService} from '../user.service';
import {UserUpdatedSignal} from '../user.signals';

@Component({
    selector: 'app-account',
    templateUrl: './account.component.html',
    styleUrls: ['./account.component.scss'],
})
export class AccountComponent implements OnInit {
    ui = UI;

    user: User;

    progress = {
        loadUser: false,
        updateUser: false,
    };

    form = this.fb.group({
        firstName: [null, [Validators.required]],
        lastName: [null, [Validators.required]],
        email: [null, [Validators.required, Validators.email]],
    });

    @ViewChild('formRef')
    formRef: FormComponent;

    constructor(
        private userService: UserService,
        private fb: FormBuilder,
        private signals: SignalsService,
    ) {
    }

    ngOnInit(): void {
        this.loadUser();
    }

    loadUser() {
        this.progress.loadUser = true;
        this.userService.get()
            .pipe(finalize(() => this.progress.loadUser = false))
            .subscribe(user => {
                this.user = user;
                this.form.patchValue({
                    firstName: user.firstName,
                    lastName: user.lastName,
                    email: user.email,
                });
            });
    }

    update() {
        const request = new UserUpdated(this.form.getRawValue());
        this.progress.updateUser = true;
        this.userService.update(request)
            .pipe(finalize(() => this.progress.updateUser = false))
            .subscribe((user) => {
                this.signals.dispatch(new UserUpdatedSignal(this));
                this.user = user;
                this.formRef.success();
            });
    }
}
