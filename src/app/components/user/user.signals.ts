import {ISignal} from '../../../services/signals.service';

export class UserUpdatedSignal implements ISignal {
    constructor(public owner: any) {
    }
}
