import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {EsanumUiModule, FormsModule} from '@esanum/ui';

import {OrdersRoutingModule} from './orders-routing.module';
import {OrdersComponent} from './orders.component';

@NgModule({
    declarations: [OrdersComponent],
    imports: [
        CommonModule,
        OrdersRoutingModule,
        EsanumUiModule,
        FormsModule,
        ReactiveFormsModule,
    ],
})
export class OrdersModule {
}
