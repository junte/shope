import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder} from '@angular/forms';

import {TableComponent, UI} from '@esanum/ui';
import {serialize} from '@junte/serialize-ts';
import {OrdersFilter} from '../../../../models/order';

import {OrderService} from '../../order/order.service';

export function equals(a, b) {
    return JSON.stringify(a) === JSON.stringify(b);
}

@Component({
    selector: 'app-orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.scss'],
})
export class OrdersComponent implements OnInit {
    ui = UI;

    private reset;

    filter: OrdersFilter;

    tableControl = this.fb.control({
        first: 10,
        offset: 0,
    });
    form = this.fb.group({
        table: this.tableControl,
    });

    @ViewChild('table', {static: true})
    table: TableComponent;

    constructor(
        private orderService: OrderService,
        private fb: FormBuilder,
    ) {
    }

    ngOnInit(): void {
        this.table.fetcher = () => {
            return this.orderService.getPaging();
        };

        this.form.valueChanges.subscribe(() => this.load());
        this.table.load();
    }

    private load() {
        const {table: {first}} = this.form.getRawValue();
        const filter = new OrdersFilter({first});

        const reset = serialize(filter);
        if (!!this.reset && !equals(reset, this.reset)) {
            this.tableControl.setValue({first, offset: 0}, {emitEvent: false});
        }
        this.reset = reset;

        const {table: {offset}} = this.form.getRawValue();
        filter.offset = offset;

        if (equals(filter, this.filter)) {
            return;
        }
        this.filter = filter;

        if (!!this.table) {
            this.table.load();
        }
    }
}
