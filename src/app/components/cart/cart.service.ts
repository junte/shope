import {Injectable} from '@angular/core';

import {deserialize, serialize} from '@junte/serialize-ts';
import {Observable, of} from 'rxjs';
import {delay} from 'rxjs/operators';
import {MOCKS_DELAY} from '../../../consts';

import {Cart} from '../../../models/cart';
import {Product} from '../../../models/product';

const CART_KEY = 'cart';

@Injectable({
    providedIn: 'root',
})
export class CartService {

    constructor() {
    }

    private getFromStorage(): Cart {
        return !!localStorage[CART_KEY]
            ? deserialize(JSON.parse(localStorage[CART_KEY]), Cart)
            : null;
    }

    private setToStorage(cart: Cart) {
        localStorage.setItem(CART_KEY, JSON.stringify(serialize(cart)));
    }

    private removeFromStorage(key: string) {
        localStorage.removeItem(key);
    }

    get(): Observable<Cart> {
        return of(this.getFromStorage() || new Cart())
            .pipe(delay(MOCKS_DELAY));
    }

    add(product: Product, quantity: number): Observable<Cart> {
        const cart = this.getFromStorage() || new Cart();

        const id = cart.items.findIndex((item => item.id === product.id));
        if (id !== -1) {
            cart.items[id].product = product;
            cart.items[id].quantity += quantity;
            cart.items[id].amountTotal = product.price * cart.items[id].quantity;
            cart.totalCost += product.price;
            cart.totalQuantity += quantity;
        } else {
            const amountTotal = product.price * quantity;
            cart.items.push({
                id: product.id,
                product,
                quantity,
                amountTotal,
            });
            cart.totalCost += amountTotal;
            cart.totalQuantity += quantity;
        }

        this.setToStorage(cart);

        return of(cart);
    }

    delete(id: number) {
        const cart = this.getFromStorage();
        const removedId = cart.items.findIndex((item => item.id === id));

        if (removedId !== -1) {
            cart.totalCost -= cart.items[removedId].amountTotal;
            cart.totalQuantity -= cart.items[removedId].quantity;
            cart.items.splice(removedId, 1);
        }

        if (cart.totalQuantity > 0) {
            this.setToStorage(cart);
        } else {
            this.removeFromStorage(CART_KEY);
        }
    }

    clear() {
        this.removeFromStorage(CART_KEY);
    }
}
