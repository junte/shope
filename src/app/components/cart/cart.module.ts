import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EsanumUiModule} from '@esanum/ui';

import {CartRoutingModule} from './cart-routing.module';
import {CartComponent} from './cart.component';

@NgModule({
    declarations: [CartComponent],
    imports: [
        CommonModule,
        CartRoutingModule,
        EsanumUiModule,
    ],
})
export class CartModule {
}
