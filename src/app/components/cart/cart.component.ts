import {Component, OnInit} from '@angular/core';

import {UI} from '@esanum/ui';
import {filter, finalize} from 'rxjs/operators';

import {Cart} from '../../../models/cart';
import {CartService} from './cart.service';
import {CartUpdatedSignal} from './cart.signals';
import {SignalsService} from '../../../services/signals.service';

@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {
    ui = UI;

    cart: Cart;
    loading = false;

    constructor(
        private cartService: CartService,
        private signals: SignalsService,
    ) {
    }

    ngOnInit(): void {
        this.loadCart();

        this.signals.queue
            .pipe(filter(signal => signal instanceof CartUpdatedSignal))
            .subscribe(() => this.loadCart());
    }

    loadCart() {
        this.loading = true;
        this.cartService.get()
            .pipe(finalize(() => this.loading = false))
            .subscribe(cart => this.cart = cart);
    }

    deleteItem(id: number) {
        this.cartService.delete(id);
        this.signals.dispatch(new CartUpdatedSignal(this));
    }

    clearCart() {
        this.cartService.clear();
        this.signals.dispatch(new CartUpdatedSignal(this));
    }
}
