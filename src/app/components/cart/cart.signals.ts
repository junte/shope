import {ISignal} from '../../../services/signals.service';

export class CartUpdatedSignal implements ISignal {
    constructor(public owner: any) {
    }
}
