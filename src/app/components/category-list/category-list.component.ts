import {Component, OnInit} from '@angular/core';

import {UI} from '@esanum/ui';
import {finalize} from 'rxjs/operators';

import {Category} from '../../../models/category';
import {CategoryListService} from './category-list.service';

@Component({
    selector: 'app-category-list',
    styleUrls: ['./category-list.component.scss'],
    templateUrl: './category-list.component.html',
})
export class CategoryListComponent implements OnInit {
    ui = UI;

    progress = {
        categories: false,
    };

    categories: Category[] = [];

    constructor(
        private categoryListService: CategoryListService,
    ) {
    }

    ngOnInit(): void {
        this.loadCategories();
    }

    private loadCategories(): void {
        this.progress.categories = true;

        this.categoryListService.get()
            .pipe(finalize(() => this.progress.categories = false))
            .subscribe((categories) => this.categories = categories);
    }
}
