import {Injectable} from '@angular/core';

import {getMock} from '@junte/mocker';
import {deserialize} from '@junte/serialize-ts';
import {Observable, of} from 'rxjs';
import {delay, map} from 'rxjs/operators';

import {Category} from '../../../models/category';
import {MOCKS_DELAY, USE_MOCKS} from '../../../consts';
import {AllCategoriesGQL} from './category-list.graphql';

@Injectable({
    providedIn: 'root',
})
export class CategoryListService {

    constructor(
        private allCategoriesGQL: AllCategoriesGQL,
    ) {
    }

    get(): Observable<Category[]> {
        if (USE_MOCKS) {
            return of(Array.from(Array(7)).map(() => getMock(Category)))
                .pipe(delay(MOCKS_DELAY));
        }
        return this.allCategoriesGQL.fetch()
            .pipe(map(({data: {categories}}) => categories.map(item => deserialize(item, Category))));
    }
}
