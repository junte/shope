import {Injectable} from '@angular/core';

import {Query} from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
    providedIn: 'root',
})
export class AllCategoriesGQL extends Query<{ categories }> {
    document = gql`
        query {
            categories {
                title
                slug
                image
            }
        }`;
}
