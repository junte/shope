import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EsanumUiModule} from '@esanum/ui';

import {CategoryListRoutingModule} from './category-list-routing.module';
import {CategoryListComponent} from './category-list.component';

@NgModule({
    declarations: [
        CategoryListComponent,
    ],
    imports: [
        CommonModule,
        CategoryListRoutingModule,
        EsanumUiModule,
    ],
})
export class CategoryListModule {
}
