import {Component, OnInit} from '@angular/core';

import {UI} from '@esanum/ui';
import {of} from 'rxjs';
import {filter, switchMap} from 'rxjs/operators';

import {Cart} from '../../../models/cart';
import {User} from '../../../models/user';
import {AppConfig} from '../../app.config';
import {SignalsService} from '../../../services/signals.service';
import {CartService} from '../cart/cart.service';
import {CartUpdatedSignal} from '../cart/cart.signals';
import {UserService} from '../user/user.service';
import {UserUpdatedSignal} from '../user/user.signals';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit {
    ui = UI;

    cart: Cart;
    user: User;

    constructor(
        public config: AppConfig,
        private cartService: CartService,
        private signals: SignalsService,
        private userService: UserService,
    ) {
    }

    ngOnInit(): void {
        this.loadUser();
        this.loadCart();
        this.catchSignals();
    }

    loadCart() {
        this.cartService.get()
            .subscribe(cart => this.cart = cart);
    }

    loadUser() {
        this.config.userToken$
            .pipe(switchMap(token => !!token ? this.userService.get() : of(null)))
            .subscribe(user => this.user = user);
    }

    catchSignals() {
        this.signals.queue
            .pipe(filter(signal => signal instanceof CartUpdatedSignal))
            .subscribe(() => this.loadCart());

        this.signals.queue
            .pipe(filter(signal => signal instanceof UserUpdatedSignal))
            .subscribe(() => this.loadUser());
    }
}
