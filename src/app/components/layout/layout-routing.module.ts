import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {LoggedUserGuard} from '../../../guards/logged-user-guard.service';
import {LayoutComponent} from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                loadChildren: () => import('./full-content/full-content.module').then(m => m.FullContentModule),
            },
            {
                path: 'user',
                loadChildren: () => import('../user/user.module').then(m => m.UserModule),
                canActivate: [LoggedUserGuard],
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LayoutRoutingModule {
}
