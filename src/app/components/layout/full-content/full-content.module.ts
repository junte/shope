import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EsanumUiModule} from '@esanum/ui';

import {FullContentRoutingModule} from './full-content-routing.module';
import {FullContentComponent} from './full-content.component';

@NgModule({
    declarations: [FullContentComponent],
    imports: [
        CommonModule,
        FullContentRoutingModule,
        EsanumUiModule,
    ],
})
export class FullContentModule {
}
