import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AnonUserGuard} from '../../../../guards/anon-user.guard';

import {FullContentComponent} from './full-content.component';

const routes: Routes = [
    {
        path: '',
        component: FullContentComponent,
        children: [
            {
                path: '',
                loadChildren: () => import('../../category-list/category-list.module').then(m => m.CategoryListModule),
            },
            {
                path: 'cart',
                loadChildren: () => import('../../cart/cart.module').then(m => m.CartModule),
            },
            {
                path: 'orders/:order_id',
                loadChildren: () => import('../../order/order.module').then(m => m.OrderModule),
            },
            {
                path: 'signin',
                loadChildren: () => import('../../signin/signin.module').then(m => m.SigninModule),
                canActivate: [AnonUserGuard],
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class FullContentRoutingModule {
}
