import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-full',
    templateUrl: './full-content.component.html',
    styleUrls: ['./full-content.component.scss'],
})
export class FullContentComponent implements OnInit {

    constructor() {
    }

    ngOnInit(): void {
    }

}
