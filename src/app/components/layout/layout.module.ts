import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {EsanumUiModule, ShortcutsModule} from '@esanum/ui';

import {LayoutRoutingModule} from './layout-routing.module';
import {LayoutComponent} from './layout.component';
import {LoginComponent} from '../login/login.component';
import { UserMenuComponent } from './user-menu/user-menu.component';
import { CartActionMenuComponent } from './cart-action-menu/cart-action-menu.component';

@NgModule({
    declarations: [
        LayoutComponent,
        LoginComponent,
        UserMenuComponent,
        CartActionMenuComponent,
    ],
    imports: [
        CommonModule,
        LayoutRoutingModule,
        EsanumUiModule,
        FormsModule,
        ReactiveFormsModule,
        ShortcutsModule,
    ],
    exports: [
        LoginComponent,
    ],
})
export class LayoutModule {
}
