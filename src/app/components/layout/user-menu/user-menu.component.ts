import {Component, ComponentFactoryResolver, Injector, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {ModalService, UI} from '@esanum/ui';
import {finalize} from 'rxjs/operators';

import {User} from '../../../../models/user';
import {AppConfig} from '../../../app.config';
import {LoginComponent} from '../../login/login.component';
import {UserService} from '../../user/user.service';

@Component({
    selector: 'app-user-menu',
    templateUrl: './user-menu.component.html',
    styleUrls: ['./user-menu.component.scss'],
})
export class UserMenuComponent implements OnInit {

    ui = UI;

    progress = {
        logout: false,
    };

    @Input()
    user: User;

    @Input()
    hide;

    constructor(
        public config: AppConfig,
        private cfr: ComponentFactoryResolver,
        private injector: Injector,
        private modal: ModalService,
        private router: Router,
        private userService: UserService,
    ) {
    }

    ngOnInit(): void {
    }

    login() {
        const component = this.cfr.resolveComponentFactory(LoginComponent).create(this.injector);
        component.instance.logged
            .subscribe(() => this.modal.close());
        this.modal.open(component, {title: {text: 'Login'}});
    }

    logout(hide: () => void) {
        this.progress.logout = true;
        this.userService.logout()
            .pipe(finalize(() => this.progress.logout = false))
            .subscribe(() => {
                hide();
                this.router.navigate(['/'])
                    .then(() => this.config.userToken = null);
            });
    }
}
