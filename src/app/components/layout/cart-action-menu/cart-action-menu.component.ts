import {Component, Input, OnInit} from '@angular/core';

import {UI} from '@esanum/ui';

import {Cart} from '../../../../models/cart';
import {SignalsService} from '../../../../services/signals.service';
import {CartService} from '../../cart/cart.service';
import {CartUpdatedSignal} from '../../cart/cart.signals';

@Component({
    selector: 'app-cart-action-menu',
    templateUrl: './cart-action-menu.component.html',
    styleUrls: ['./cart-action-menu.component.scss'],
})
export class CartActionMenuComponent implements OnInit {

    ui = UI;

    @Input()
    cart: Cart;

    constructor(
        private cartService: CartService,
        private signals: SignalsService,
    ) {
    }

    ngOnInit(): void {
    }

    deleteCartItem(id: number) {
        this.cartService.delete(id);
        this.signals.dispatch(new CartUpdatedSignal(this));
    }

}
