import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CartActionMenuComponent} from './cart-action-menu.component';

describe('CartActionMenuComponent', () => {
    let component: CartActionMenuComponent;
    let fixture: ComponentFixture<CartActionMenuComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CartActionMenuComponent],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CartActionMenuComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
