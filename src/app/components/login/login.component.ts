import {Component, EventEmitter, Output, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, Validators} from '@angular/forms';

import {FormComponent, UI} from '@esanum/ui';
import {finalize} from 'rxjs/operators';

import {AppConfig} from '../../app.config';
import {UserCredentials} from '../../../models/user';
import {UserService} from '../user/user.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
    ui = UI;

    warnings: AbstractControl[] = [];

    progress = {
        login: false,
    };

    loginControl = this.fb.control(null, [Validators.required]);
    passwordControl = this.fb.control(null, [Validators.required]);
    form = this.fb.group({
        username: this.loginControl,
        password: this.passwordControl,
    });

    @Output()
    logged: EventEmitter<any> = new EventEmitter();

    @ViewChild(FormComponent)
    formRef: FormComponent;

    constructor(
        private fb: FormBuilder,
        private userService: UserService,
        public config: AppConfig,
    ) {
    }

    login() {
        this.progress.login = true;
        const credentials = new UserCredentials(this.form.getRawValue());
        this.userService.login(credentials)
            .pipe(finalize(() => this.progress.login = false))
            .subscribe(accessToken => {
                this.config.userToken = accessToken;
                this.logged.emit();
            });
    }

    clearForm() {
        this.loginControl.reset();
        this.passwordControl.reset();
    }

    submit() {
        this.formRef.submit();
    }
}
