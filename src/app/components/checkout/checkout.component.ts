import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Router} from '@angular/router';

import {FormComponent, UI} from '@esanum/ui';
import {finalize} from 'rxjs/operators';

import {Cart} from '../../../models/cart';
import {Order, OrderUpdated} from '../../../models/order';
import {SignalsService} from '../../../services/signals.service';
import {CartService} from '../cart/cart.service';
import {CartUpdatedSignal} from '../cart/cart.signals';
import {OrderService} from '../order/order.service';

@Component({
    selector: 'app-checkout',
    templateUrl: './checkout.component.html',
    styleUrls: ['./checkout.component.scss'],
})
export class CheckoutComponent implements OnInit {

    ui = UI;

    cart: Cart;
    loading: boolean;

    form = this.fb.group({
        name: [null, [Validators.required]],
        lastName: [null, [Validators.required]],
        email: [null, [Validators.required, Validators.email]],
    });

    @ViewChild('formRef')
    formRef: FormComponent;

    constructor(
        private cartService: CartService,
        private fb: FormBuilder,
        private orderService: OrderService,
        private router: Router,
        private signals: SignalsService,
    ) {
    }

    ngOnInit() {
        this.loading = true;
        this.cartService.get()
            .pipe(finalize(() => this.loading = false))
            .subscribe(cart => this.cart = cart);
    }

    addOrder() {
        this.loading = true;
        const order = new Order(this.form.getRawValue());
        order.cart = this.cart;
        this.orderService.add(order)
            .pipe(finalize(() => this.loading = false))
            .subscribe((o: OrderUpdated) => {
                this.cartService.clear();
                this.signals.dispatch(new CartUpdatedSignal(this));

                this.formRef.success();
                this.router.navigate([`/orders/${o.id}`]);
            });
    }
}
