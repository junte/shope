import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {EsanumUiModule} from '@esanum/ui';

import {CheckoutRoutingModule} from './checkout-routing.module';
import {CheckoutComponent} from './checkout.component';

@NgModule({
    declarations: [CheckoutComponent],
    imports: [
        CommonModule,
        CheckoutRoutingModule,
        EsanumUiModule,
        FormsModule,
        ReactiveFormsModule,
    ],
})
export class CheckoutModule {
}
