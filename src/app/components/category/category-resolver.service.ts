import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';

import {Observable} from 'rxjs';

import {Category} from '../../../models/category';
import {CategoryService} from './category.service';

@Injectable({
    providedIn: 'root',
})
export class CategoryResolver implements Resolve<Category> {
    constructor(
        private categoryService: CategoryService,
    ) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Category> {
        const {category_slug: slug} = route.params as { category_slug: string };

        return this.categoryService.get(slug);
    }
}
