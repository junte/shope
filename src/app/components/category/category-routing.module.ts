import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {CategoryComponent} from './category.component';
import {CategoryResolver} from './category-resolver.service';

const routes: Routes = [
    {
        path: '',
        component: CategoryComponent,
        resolve: {
            category: CategoryResolver,
        },
    },
    {
        path: ':product_id',
        loadChildren: () => import('../../components/product/product.module').then(m => m.ProductModule),
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CategoryRoutingModule {
}
