import {Injectable} from '@angular/core';

import {getMock} from '@junte/mocker';
import {deserialize} from '@junte/serialize-ts';
import {Observable, of} from 'rxjs';
import {delay, map} from 'rxjs/operators';

import {Category} from '../../../models/category';
import {Product} from '../../../models/product';
import {MOCKS_DELAY, USE_MOCKS} from '../../../consts';
import {CategoryGQL} from './category.graphql';

@Injectable({
    providedIn: 'root',
})
export class CategoryService {

    constructor(
        private categoryGQL: CategoryGQL,
    ) {
    }

    get(slug: string): Observable<Category> {
        if (USE_MOCKS) {
            return of(getMock(Category, {slug}))
                .pipe(delay(MOCKS_DELAY));
        }
        return this.categoryGQL.fetch({slug})
            .pipe(map(({data: {category}}) => deserialize(category, Category)));
    }

    getProducts(slug: string): Observable<Product[]> {
        if (USE_MOCKS) {
            return of(Array.from(Array(8)).map(() => getMock(Product, {slug})))
                .pipe(delay(MOCKS_DELAY));
        }
        return this.categoryGQL.fetch({slug})
            .pipe(map(({data: {category: {products}}}) => products.map(item => deserialize(item, Product))));
    }
}
