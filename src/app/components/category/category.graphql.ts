import {Injectable} from '@angular/core';

import {Query} from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
    providedIn: 'root',
})
export class CategoryGQL extends Query<{ category }> {
    document = gql`
        query ($slug: String!) {
            category: categoryBySlug(slug: $slug) {
                title
                slug
                image
                products {
                    id
                    title
                    image
                    description
                    price
                }
            }
        }`;
}
