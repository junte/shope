import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {UI} from '@esanum/ui';
import {finalize} from 'rxjs/operators';
import {Category} from '../../../models/category';

import {Product} from '../../../models/product';
import {CategoryService} from './category.service';

@Component({
    selector: 'app-product-list',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.scss'],
})
export class CategoryComponent implements OnInit {
    ui = UI;

    progress = {
        products: false,
    };

    category: Category;
    products: Product[] = [];

    constructor(
        private categoryService: CategoryService,
        private route: ActivatedRoute,
    ) {
    }

    ngOnInit() {
        this.route.data.subscribe(({category}) => {
            this.category = category;
            this.loadProducts();
        });
    }

    loadProducts(): void {
        this.progress.products = true;

        this.categoryService.getProducts(this.category.slug)
            .pipe(finalize(() => this.progress.products = false))
            .subscribe((products) => this.products = products);
    }
}
