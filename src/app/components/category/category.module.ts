import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EsanumUiModule} from '@esanum/ui';

import {CategoryRoutingModule} from './category-routing.module';
import {CategoryComponent} from './category.component';

@NgModule({
    declarations: [
        CategoryComponent,
    ],
    imports: [
        CommonModule,
        CategoryRoutingModule,
        EsanumUiModule,
    ],
})
export class CategoryModule {
}
