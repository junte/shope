import {Injectable} from '@angular/core';
import {getMock} from '@junte/mocker';
import {deserialize} from '@junte/serialize-ts';

import {Observable, of} from 'rxjs';
import {delay, map} from 'rxjs/operators';

import {MOCKS_DELAY, USE_MOCKS} from '../../../consts';
import {Product} from '../../../models/product';
import {ProductGQL} from './product.graphql';

@Injectable({
    providedIn: 'root',
})
export class ProductService {

    constructor(
        private productGQL: ProductGQL,
    ) {
    }

    get(id: string): Observable<Product> {
        if (USE_MOCKS) {
            return of(getMock(Product, {id}))
                .pipe(delay(MOCKS_DELAY));
        }
        return this.productGQL.fetch({id})
            .pipe(map(({data: {product}}) => deserialize(product, Product)));
    }
}
