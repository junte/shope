import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';

import {Observable} from 'rxjs';

import {Product} from '../../../models/product';
import {ProductService} from './product.service';

@Injectable({
    providedIn: 'root',
})
export class ProductResolver implements Resolve<Product> {
    constructor(
        private productService: ProductService,
    ) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Product> {
        const {product_id: id} = route.params as { product_id: string };

        return this.productService.get(id);
    }
}
