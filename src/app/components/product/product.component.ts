import {Component, OnInit} from '@angular/core';
import {ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {BlockComponent, UI} from '@esanum/ui';

import {Product} from '../../../models/product';
import {SignalsService} from '../../../services/signals.service';
import {CartService} from '../cart/cart.service';
import {CartUpdatedSignal} from '../cart/cart.signals';

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {
    ui = UI;

    product: Product;

    @ViewChild('block')
    block: BlockComponent;

    constructor(
        private route: ActivatedRoute,
        private cartService: CartService,
        private signals: SignalsService,
    ) {
    }

    ngOnInit(): void {
        this.route.data.subscribe(({product}) => this.product = product);
    }

    addToCart(product: Product) {
        this.cartService.add(product, 1);
        this.signals.dispatch(new CartUpdatedSignal(this));
        this.block.success();
    }
}
