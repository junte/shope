import {Injectable} from '@angular/core';

import {Query} from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
    providedIn: 'root',
})
export class ProductGQL extends Query<{ product }> {
    document = gql`
        query ($id: ID!) {
            product(id: $id) {
                id
                title
                description
                image
                price
            }
        }`;
}
