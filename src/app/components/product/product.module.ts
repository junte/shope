import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EsanumUiModule} from '@esanum/ui';

import {ProductRoutingModule} from './product-routing.module';
import {ProductComponent} from './product.component';

@NgModule({
    declarations: [ProductComponent],
    imports: [
        CommonModule,
        ProductRoutingModule,
        EsanumUiModule,
    ],
})
export class ProductModule {
}
