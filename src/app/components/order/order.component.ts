import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {UI} from '@esanum/ui';

import {OrderUpdated} from '../../../models/order';

@Component({
    selector: 'app-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss'],
})
export class OrderComponent implements OnInit {
    ui = UI;

    order: OrderUpdated;

    constructor(
        private route: ActivatedRoute,
    ) {
    }

    ngOnInit(): void {
        this.route.data.subscribe(({order}) => this.order = order);
    }

}
