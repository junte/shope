import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EsanumUiModule} from '@esanum/ui';

import {OrderRoutingModule} from './order-routing.module';
import {OrderComponent} from './order.component';

@NgModule({
    declarations: [OrderComponent],
    imports: [
        CommonModule,
        OrderRoutingModule,
        EsanumUiModule,
    ],
})
export class OrderModule {
}
