import {Injectable} from '@angular/core';
import {getMock} from '@junte/mocker';

import {serialize} from '@junte/serialize-ts';
import {Observable, of} from 'rxjs';
import {delay} from 'rxjs/operators';

import {MOCKS_DELAY} from '../../../consts';
import {Order, OrderUpdated, PagingOrders} from '../../../models/order';

@Injectable({
    providedIn: 'root',
})
export class OrderService {

    constructor() {
    }

    private saveOrder(order: Order): OrderUpdated {
        const orderUpdated = getMock(OrderUpdated);
        Object.assign(orderUpdated, order);

        localStorage.setItem(`order${orderUpdated.id}`, JSON.stringify(serialize(orderUpdated)));

        return orderUpdated;
    }

    private getOrder(id: string): OrderUpdated {
        const item = localStorage.getItem(`order${id}`);

        return item ? JSON.parse(localStorage.getItem(`order${id}`)) : getMock(OrderUpdated);
    }

    add(order: Order): Observable<OrderUpdated> {
        return of(this.saveOrder(order))
            .pipe(delay(MOCKS_DELAY));
    }

    get(id: string): Observable<OrderUpdated> {
        return of(this.getOrder(id))
            .pipe(delay(MOCKS_DELAY));
    }

    getPaging(): Observable<PagingOrders> {
        return of(getMock(PagingOrders))
            .pipe(delay(MOCKS_DELAY));
    }
}
