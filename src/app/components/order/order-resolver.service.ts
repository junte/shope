import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';

import {Observable} from 'rxjs';

import {OrderUpdated} from '../../../models/order';
import {OrderService} from './order.service';

@Injectable({
    providedIn: 'root',
})
export class OrderResolver implements Resolve<OrderUpdated> {
    constructor(
        private orderService: OrderService,
    ) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<OrderUpdated> {
        const {order_id: id} = route.params as { order_id: string };

        return this.orderService.get(id);
    }
}
