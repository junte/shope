import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {OrderResolver} from './order-resolver.service';
import {OrderComponent} from './order.component';

const routes: Routes = [
    {
        path: '',
        component: OrderComponent,
        resolve: {
            order: OrderResolver,
        },
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class OrderRoutingModule {
}
