import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {UI} from '@esanum/ui';

@Component({
    selector: 'app-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.scss'],
})
export class SigninComponent implements OnInit {
    ui = UI;

    constructor(
        private router: Router,
    ) {
    }

    ngOnInit(): void {
    }

    redirect() {
        this.router.navigate(['/']);
    }
}
