import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EsanumUiModule} from '@esanum/ui';

import {LayoutModule} from '../layout/layout.module';
import {SigninRoutingModule} from './signin-routing.module';
import {SigninComponent} from './signin.component';

@NgModule({
    declarations: [SigninComponent],
    imports: [
        CommonModule,
        SigninRoutingModule,
        LayoutModule,
        EsanumUiModule,
    ],
})
export class SigninModule {
}
