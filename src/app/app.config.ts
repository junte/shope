import { Injectable } from '@angular/core';

import {BehaviorSubject} from 'rxjs';

import {AccessToken} from '../models/access-token';

const USER_TOKEN = 'userToken';

@Injectable({
    providedIn: 'root',
})
export class AppConfig {
    userToken$: BehaviorSubject<AccessToken> = new BehaviorSubject<AccessToken>((() => {
        if (!!localStorage.userToken) {
            return JSON.parse(localStorage.userToken) as AccessToken;
        }

        return null;
    })());

    set userToken(accessToken: AccessToken) {
        if (!!accessToken) {
            localStorage.setItem(USER_TOKEN, JSON.stringify(accessToken));
        } else {
            localStorage.removeItem(USER_TOKEN);
        }

        this.userToken$.next(accessToken);
    }

    get userToken() {
        return this.userToken$.getValue();
    }
}
