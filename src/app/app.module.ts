import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {EsanumUiModule} from '@esanum/ui';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import { GraphQLModule } from './graphql.module';

@NgModule({
    bootstrap: [AppComponent],
    declarations: [
        AppComponent,
    ],
    imports: [
        AppRoutingModule,
        BrowserAnimationsModule,
        BrowserModule,
        HttpClientModule,
        EsanumUiModule.forRoot(),
        GraphQLModule,
    ],
    providers: [],
})
export class AppModule {
}
