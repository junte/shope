import {getMock, MockField} from '@junte/mocker';
import {ArraySerializer, Field, Model} from '@junte/serialize-ts';
import {ModelMetadataSerializer} from '@junte/serialize-ts/dist/serializers/model-metadata.serializer';

import {CartItem} from './cart-item';
import {mocks} from './utils';

@Model()
export class Cart {

    @Field({serializer: new ArraySerializer(new ModelMetadataSerializer(CartItem))})
    @MockField(() => Array.from({length: mocks.random.num(1, 5)}, () => getMock(CartItem)))
    items: CartItem[] = [];

    @Field()
    @MockField(() => mocks.random.num(1000, 100000))
    totalCost: number = 0;

    @Field()
    @MockField(() => mocks.random.num(10, 100))
    totalQuantity: number = 0;
}
