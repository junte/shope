import {MockField} from '@junte/mocker';
import {Field, Model} from '@junte/serialize-ts';

import {mocks} from './utils';

@Model()
export class User {

    @Field()
    @MockField(() => mocks.random.num(1, 100))
    id: number;

    @Field()
    @MockField( () => mocks.user.firstName())
    firstName: string;

    @Field()
    @MockField( () => mocks.user.lastName())
    lastName: string;

    @Field()
    @MockField(() => mocks.user.email())
    email: string;

    @Field()
    @MockField(() => 'https://placeimg.com/400/400/people')
    avatar: string;

    constructor(defs: Partial<UserCredentials> = {}) {
        Object.assign(this, defs);
    }
}

@Model()
export class UserUpdated {

    @Field()
    @MockField( () => mocks.user.firstName())
    firstName: string;

    @Field()
    @MockField( () => mocks.user.lastName())
    lastName: string;

    @Field()
    @MockField(() => mocks.user.email())
    email: string;

    constructor(defs: Partial<UserCredentials> = {}) {
        Object.assign(this, defs);
    }
}

@Model()
export class UserCredentials {

    @Field()
    @MockField(() => 'luke')
    username: string;

    @Field()
    @MockField(() => '123456')
    password: string;

    constructor(defs: Partial<UserCredentials> = {}) {
        Object.assign(this, defs);
    }
}
