import {MockField} from '@junte/mocker';
import {Field, Model} from '@junte/serialize-ts';

import {Category} from './category';
import {mocks} from './utils';

@Model()
export class Product {

    @Field()
    @MockField((context) => context?.id || mocks.random.num(1, 100))
    id: number;

    @Field()
    @MockField(Category)
    category: Category;

    @Field()
    @MockField(() => mocks.product.title())
    title: string;

    @Field()
    @MockField(() => mocks.product.description)
    description: string;

    @Field({jsonPropertyName: 'image'})
    @MockField(() => mocks.random.image())
    img: string;

    @Field()
    @MockField(() => mocks.random.num(1000, 100000))
    price: number;
}
