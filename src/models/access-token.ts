import {MockField} from '@junte/mocker';
import {Field, Model} from '@junte/serialize-ts';

import {mocks} from './utils';

@Model()
export class AccessToken {

    @Field({jsonPropertyName: 'access-token'})
    @MockField(() => mocks.token.hash)
    accessToken: string;

    @Field({jsonPropertyName: 'token-type'})
    @MockField(() => 'Bearer')
    tokenType: string;
}
