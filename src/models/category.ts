import {MockField} from '@junte/mocker';
import {Field, Model} from '@junte/serialize-ts';

import {mocks} from './utils';

@Model()
export class Category {
    @Field()
    @MockField(() => mocks.category.title())
    title: string;

    @Field()
    @MockField(() => mocks.category.slug())
    slug: string;

    @Field({jsonPropertyName: 'image'})
    @MockField(() => mocks.random.image())
    img: string;
}
