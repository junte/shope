import {MockField} from '@junte/mocker';
import {Field, Model} from '@junte/serialize-ts';

import {Product} from './product';
import {mocks} from './utils';

@Model()
export class CartItem {

    @Field()
    @MockField(() => mocks.random.num(1, 100))
    id: number;

    @Field()
    @MockField(Product)
    product: Product;

    @Field()
    @MockField(() => mocks.random.num(1, 10))
    quantity: number;

    @Field()
    @MockField(() => mocks.random.num(10000, 100000))
    amountTotal: number;
}
