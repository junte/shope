export function randomFromArray(arr: string[]): string {
    return arr[Math.floor(Math.random() * arr.length)];
}

export const mocks = {
    random: {
        fromArray: (arr: string[]) => {
            return randomFromArray(arr);
        },
        num: (min: number, max: number) => {
            return Math.round(min + Math.random() * (max - min + 1));
        },
        image: () => randomFromArray([
            'https://placeimg.com/600/600/tech',
            'https://placeimg.com/600/600/arch',
            'https://placeimg.com/600/600/nature',
        ]),
    },
    category: {
        title: () => randomFromArray(['Category X', 'Category Y', 'Category Z']),
        slug: () => randomFromArray(['category-x', 'category-y', 'category-z']),
    },
    product: {
        title: () => randomFromArray(['Product X', 'Product Y', 'Product Z']),
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean cursus elit et massa maximus suscipit. ' +
            'Ut vitae felis tortor. Vestibulum molestie quis sapien eu laoreet. Nulla eget tempor nisi. ' +
            'Etiam mollis, est nec aliquam commodo, dolor eros volutpat orci, vel vulputate sapien diam sit amet purus. ' +
            'Suspendisse blandit odio at consectetur tincidunt. Integer sed est quam. ' +
            'Ut arcu libero, consequat ac malesuada vel, ornare a dolor. Quisque finibus ante magna. ' +
            'Ut sed sollicitudin elit, vel cursus massa. Etiam molestie justo eu neque tincidunt, non aliquam ligula finibus. ' +
            'Aliquam egestas neque vitae nisi malesuada, a rutrum augue rutrum.',
    },
    order: {
        status: () => randomFromArray(['Accepted', 'Completed', 'Shipped']),
    },
    user: {
        firstName: () => randomFromArray(['Luke', 'Han', 'Leia']),
        lastName: () => randomFromArray(['Skywalker', 'Solo', 'Organa']),
        email: () => randomFromArray(['luke@sw.com', 'han@sw.com', 'leia@sw.com']),
    },
    token: {
        hash: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
    },
};
