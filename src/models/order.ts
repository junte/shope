import {getMock, MockField} from '@junte/mocker';
import {ArraySerializer, Field, Model} from '@junte/serialize-ts';
import {ModelMetadataSerializer} from '@junte/serialize-ts/dist/serializers/model-metadata.serializer';

import {Cart} from './cart';
import {CartItem} from './cart-item';
import {mocks} from './utils';

@Model()
export class Order {
    @Field()
    cart: Cart;

    @Field()
    name: string;

    @Field()
    lastName: string;

    @Field()
    email: string;

    constructor(defs: Partial<Order> = {}) {
        Object.assign(this, defs);
    }
}

@Model()
export class OrderUpdated {
    @Field()
    @MockField(() => mocks.random.num(1, 100))
    id: number;

    @Field()
    @MockField(() => mocks.order.status())
    status: string;

    @Field({serializer: new ArraySerializer(new ModelMetadataSerializer(CartItem))})
    @MockField(() => Array.from({length: mocks.random.num(1, 7)}, () => getMock(CartItem)))
    items: CartItem[];

    @Field()
    @MockField(() => mocks.random.num(1000, 100000))
    totalCost: number;

    @Field()
    @MockField(() => mocks.random.num(10, 100))
    totalQuantity: number;

    @Field()
    @MockField( () => mocks.user.firstName())
    name: string;

    @Field()
    @MockField( () => mocks.user.lastName())
    lastName: string;

    @Field()
    @MockField(() => mocks.user.email())
    email: string;

    constructor(defs: Partial<Order> = {}) {
        Object.assign(this, defs);
    }
}

@Model()
export class PagingOrders {

    @Field()
    @MockField(() => 7)
    count: number;

    @Field({serializer: new ArraySerializer(new ModelMetadataSerializer(OrderUpdated))})
    @MockField(() => Array.from({length: 7}, () => getMock(OrderUpdated)))
    results: OrderUpdated[];
}

@Model()
export class OrdersFilter {

    @Field()
    first: number;

    @Field()
    offset: number;

    constructor(defs: Partial<OrdersFilter> = null) {
        Object.assign(this, defs);
    }
}
