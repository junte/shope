import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {BACKEND_ENDPOINT} from '../consts';

function getRequestUrl(path: string): string {
    return [BACKEND_ENDPOINT, path].join('/');
}

@Injectable({
    providedIn: 'root',
})
export class HttpService {

    constructor(
        private http: HttpClient,
    ) {
    }

    get(path: string): Observable<any | any[]> {
        return this.http.get(getRequestUrl(path));
    }
}
