import {Injectable} from '@angular/core';

import {Subject} from 'rxjs';

export interface ISignal {
    owner: any;
}

@Injectable({
    providedIn: 'root',
})
export class SignalsService {

    queue = new Subject<ISignal>();

    dispatch(signal: ISignal) {
        this.queue.next(signal);
    }

}
