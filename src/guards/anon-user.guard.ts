import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree} from '@angular/router';

import {AppConfig} from '../app/app.config';

@Injectable({
    providedIn: 'root',
})
export class AnonUserGuard implements CanActivate {
    constructor(
        private router: Router,
        private config: AppConfig,
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): UrlTree | boolean {
        return !this.config.userToken ? true : this.router.createUrlTree(['/']);
    }
}
