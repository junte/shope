export const USE_MOCKS = false;
export const MOCKS_DELAY = 2000;

export const BACKEND_ENDPOINT = 'http://localhost:1337';
export const GRAPHQL_ENDPOINT = 'http://localhost:1337/graphql';
