import {browser, element} from 'protractor';
import {protractor} from 'protractor/built/ptor';

export const EC = protractor.ExpectedConditions;
const WAIT_ELEMENT_TIMEOUT = 15000;

export function isPresentElement(locator) {
    return browser.wait(() => element(locator).isPresent(), WAIT_ELEMENT_TIMEOUT);
}

export function isPresent(locator) {
    browser.wait(() => locator.isPresent(), WAIT_ELEMENT_TIMEOUT);
}

export function getFirstElement(locator) {
    browser.sleep(1000);
    return element.all(locator).first();
}

export function click(locator) {
    browser.wait(EC.elementToBeClickable(locator), WAIT_ELEMENT_TIMEOUT);
    browser.actions().mouseMove(locator).click().perform();
}
