import {browser} from 'protractor';
import {ProductPage} from './product.po';

describe('Product', () => {

    const productPage: ProductPage = new ProductPage();

    beforeEach(() => {
        browser.get('/categories/category-x/1');

        productPage.checkProductPresent();
    });

    it('check product block present', () => {
        expect(productPage.checkProductBlockTitlePresent().isPresent()).toBeTruthy();
        expect(productPage.checkProductBlockImagePresent().isPresent()).toBeTruthy();
        expect(productPage.checkProductBlockDescriptionPresent().isPresent()).toBeTruthy();
        expect(productPage.checkProductBlockPricePresent().isPresent()).toBeTruthy();
        expect(productPage.checkProductBlockAddToCartButtonPresent().isPresent()).toBeTruthy();
    });
});
