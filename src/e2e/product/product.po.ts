import {by, element} from 'protractor';
import {isPresentElement} from '../utils';

const PRODUCT_LOCATOR = by.css('app-product');
const PRODUCT_BLOCK_TITLE_LOCATOR = by.css('app-product sn-block [data-title]');
const PRODUCT_BLOCK_IMAGE_LOCATOR = by.css('app-product sn-block sn-picture');
const PRODUCT_BLOCK_DESCRIPTION_LOCATOR = by.css('app-product sn-block [data-description]');
const PRODUCT_BLOCK_PRICE_LOCATOR = by.css('app-product sn-block [data-price]');
const PRODUCT_BLOCK_ADD_TO_CART_BUTTON_LOCATOR = by.css('app-product sn-block [data-add]');

export class ProductPage {
    checkProductPresent() {
        isPresentElement(PRODUCT_LOCATOR);
    }

    checkProductBlockTitlePresent() {
        isPresentElement(PRODUCT_BLOCK_TITLE_LOCATOR);
        return element(PRODUCT_BLOCK_TITLE_LOCATOR);
    }

    checkProductBlockImagePresent() {
        isPresentElement(PRODUCT_BLOCK_IMAGE_LOCATOR);
        return element(PRODUCT_BLOCK_IMAGE_LOCATOR);
    }

    checkProductBlockDescriptionPresent() {
        isPresentElement(PRODUCT_BLOCK_DESCRIPTION_LOCATOR);
        return element(PRODUCT_BLOCK_DESCRIPTION_LOCATOR);
    }

    checkProductBlockPricePresent() {
        isPresentElement(PRODUCT_BLOCK_PRICE_LOCATOR);
        return element(PRODUCT_BLOCK_PRICE_LOCATOR);
    }

    checkProductBlockAddToCartButtonPresent() {
        isPresentElement(PRODUCT_BLOCK_ADD_TO_CART_BUTTON_LOCATOR);
        return element(PRODUCT_BLOCK_ADD_TO_CART_BUTTON_LOCATOR);
    }
}
