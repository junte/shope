import {by, element} from 'protractor';

import {click, getFirstElement, isPresent, isPresentElement} from '../utils';

const CATEGORY_LIST_LOCATOR = by.css('app-category-list');
const CATEGORY_CARD_LOCATOR = by.css('app-category-list sn-card');
const CATEGORY_CARD_IMAGE_LOCATOR = by.css('app-category-list sn-card [data-picture]');
const CATEGORY_CARD_TITLE_LOCATOR = by.css('app-category-list sn-card [data-title] [data-text]');
const PRODUCT_LIST_LOCATOR = by.css('app-product-list');

export class CategoryListPage {
    checkCategoryListPresent() {
        isPresentElement(CATEGORY_LIST_LOCATOR);
    }

    checkCategoryCardImagePresent() {
        isPresent(getFirstElement(CATEGORY_CARD_IMAGE_LOCATOR));
        return element(CATEGORY_CARD_IMAGE_LOCATOR);
    }

    checkCategoryCardTitlePresent() {
        isPresent(getFirstElement(CATEGORY_CARD_TITLE_LOCATOR));
        return element(CATEGORY_CARD_TITLE_LOCATOR);
    }

    goToCategoryByCardClick() {
        isPresent(getFirstElement(CATEGORY_CARD_LOCATOR));
        click(getFirstElement(CATEGORY_CARD_LOCATOR));
    }

    checkProductListPresent() {
        isPresentElement(PRODUCT_LIST_LOCATOR);
        return element(PRODUCT_LIST_LOCATOR);
    }
}
