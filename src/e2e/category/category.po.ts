import {by, element} from 'protractor';

import {click, getFirstElement, isPresent, isPresentElement} from '../utils';

const PRODUCT_LIST_LOCATOR = by.css('app-product-list');
const PRODUCT_CARD_LOCATOR = by.css('app-product-list sn-card');
const PRODUCT_CARD_IMAGE_LOCATOR = by.css('app-product-list sn-card [data-picture]');
const PRODUCT_CARD_TITLE_LOCATOR = by.css('app-product-list sn-card [data-title] [data-text]');
const PRODUCT_DETAIL_LOCATOR = by.css('app-product');

export class CategoryPage {
    checkProductListPresent() {
        isPresentElement(PRODUCT_LIST_LOCATOR);
    }

    checkProductCardImagePresent() {
        isPresent(getFirstElement(PRODUCT_CARD_IMAGE_LOCATOR));
        return element(PRODUCT_CARD_IMAGE_LOCATOR);
    }

    checkProductCardTitlePresent() {
        isPresent(getFirstElement(PRODUCT_CARD_TITLE_LOCATOR));
        return element(PRODUCT_CARD_TITLE_LOCATOR);
    }

    goToProductDetailByCardClick() {
        isPresent(getFirstElement(PRODUCT_CARD_LOCATOR));
        click(getFirstElement(PRODUCT_CARD_LOCATOR));
    }

    checkProductDetailPresent() {
        isPresentElement(PRODUCT_DETAIL_LOCATOR);
        return element(PRODUCT_DETAIL_LOCATOR);
    }
}
