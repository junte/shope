import {browser} from 'protractor';
import {CategoryPage} from './category.po';

describe('Category', () => {

    const categoryPage: CategoryPage = new CategoryPage();

    beforeEach(() => {
        browser.get('/categories/category-x');

        categoryPage.checkProductListPresent();
    });

    it('check product cards present', () => {
        expect(categoryPage.checkProductCardImagePresent().isPresent()).toBeTruthy();
        expect(categoryPage.checkProductCardTitlePresent().isPresent()).toBeTruthy();
    });

    it('go to product detail by product card clicking', () => {
        categoryPage.goToProductDetailByCardClick();
        expect(categoryPage.checkProductDetailPresent().isPresent()).toBeTruthy();
    });
});
